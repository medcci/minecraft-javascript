var $minecraft = (function() {
  var baseUrl = "https://craftapi.com/api/server/info/",
    ajaxQuery, fn = {};

  ajaxQuery = function(parseURL, callback) {
    var ajax = new XMLHttpRequest();
    callback(ajax);

    ajax.open("GET", baseUrl + parseURL, true);
    ajax.send();
  };

  fn.gI = function(requestUrl, options) {
    var options = options || {};
    var port = options.port || 25565;
    var success = options.success || success();
    var fail = options.fail || fail();
    var address = requestUrl + ":" + port;

    ajaxQuery(address, function(req) {
      req.onerror = function() {
        fail(true);
      };

      req.onload = function() {
        var dataJ;
        try {
          dataJ = JSON.parse(req.responseText);
          if (dataJ.error) {
            fail(dataJ.error);
          }
        } catch (e) {
          fail(e);
        }
        success(dataJ);
        fail(undefined);
      };
    });
  };

  return {
    Query: fn.gI
  };
}());
